package verification;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PublicationsVerification {
    @Step("Verify received publicationId is {1}")
    public void verifyPublicationId(Response lastResponse, String publicationId){
        assertTrue(lastResponse.getBody().jsonPath().getList("data.id").contains(publicationId));
    }
}
