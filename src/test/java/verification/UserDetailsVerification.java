package verification;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.junit.jupiter.api.Assertions;

public class UserDetailsVerification {
    public String getUserId(Response response){
        return response.getBody().jsonPath().getString("data.id");
    }
    @Step("Verify received userId is {1}")
    public void verifyUserId(Response lastResponse, String value) {
        Assertions.assertEquals(value, getUserId(lastResponse));
    }
}
