package verification;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Verification {
    @Step("Verify that API response is {0}")
    public void verifyStatusCode(Integer statusCode, Response lastResponse) {
        assertEquals(statusCode, lastResponse.getStatusCode());
    }
    @Step("Verify response schema with {0}")
    public void verifyResponseSchema(String jsonSchema, Response lastResponse) {
        lastResponse.then().body(matchesJsonSchemaInClasspath("schema/" + jsonSchema));
    }
    public String getErrorMessage(Response response){
        return response.getBody().jsonPath().getString("errors[0].message");
    }
    public Integer getErrorCode(Response response){
        return response.getBody().jsonPath().getInt("errors[0].code");
    }
    @Step("Verify error message is {1}")
    public void verifyMessage(Response response, String message) {
        assertEquals(message, getErrorMessage(response));
    }
    @Step("Verify error code is {1}")
    public void verifyCode(Response response, Integer code) {
        assertEquals(code, getErrorCode(response));
    }
}
