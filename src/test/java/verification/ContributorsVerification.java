package verification;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ContributorsVerification {
    @Step("Verify received contributor ID is {1}")
    public void verifyPublicationID(Response lastResponse, String value) {
        assertTrue(lastResponse.getBody().jsonPath().getList("data.publicationId").contains(value));
    }

    @Step("Verify received data is empty")
    public void verifyListIsEmpty(Response lastResponse) {
        assertTrue(lastResponse.getBody().jsonPath().getString("data").equals("[]"));
    }
}
