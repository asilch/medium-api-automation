package data;

public enum AccessToken {
    valid("2d827027b5029289151c457a2d7c1c7aceccbbfe9a0d2966799f669de99e5e761"),
    invalid("3d827027b5029289151c457a2d7c1c7aceccbbfe9a0d2966799f669de99e5e761"),
    no("");
    private String value;

    AccessToken(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
