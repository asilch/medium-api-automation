package data;

public enum UserId {
    valid("12f1d0ff9760bc92a3564531cb09adee5b408156189bd5c60ec3d523d23747b2c"),
    anotherUserId("17d52464455e37d8180f957b65bc41247c8f415445e71a1f2ebc16c64206e777d"),
    invalid("1");
    private String value;

    UserId(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
