package stepDefs;

import request.Request;
import verification.ContributorsVerification;
import io.cucumber.java.en.*;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class ContributorsStep {
    @Steps
    Request request;

    @Steps
    ContributorsVerification verification;

    @When("I try to get a list of contributors for publication with ID {string}")
    public void iTryToGetAListOfContributorsForPublicationWithID(String publicationId) {
        Serenity.setSessionVariable("publicationId").to(publicationId);
        request.get(Serenity.sessionVariableCalled("accessToken"), "/publications/" + publicationId + "/contributors");
    }

    @And("An empty list of contributors should be retrieved")
    public void anEmptyListOfContributorsShouldBeRetrieved() {
        verification.verifyListIsEmpty(lastResponse());
    }


    @And("Correct publication ID should be retrieved")
    public void correctPublicationIDShouldBeRetrieved() {
        verification.verifyPublicationID(lastResponse(), Serenity.sessionVariableCalled("publicationId"));
    }
}
