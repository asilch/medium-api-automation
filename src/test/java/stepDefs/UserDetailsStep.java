package stepDefs;

import data.UserId;
import request.Request;
import verification.UserDetailsVerification;
import verification.Verification;
import io.cucumber.java.en.*;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class UserDetailsStep {
    @Steps
    Request request;

    @Steps
    UserDetailsVerification userDetailsVerification;

    @When("I try to get user details")
    public void iTryToGetUserDetails() {
        request.get(Serenity.sessionVariableCalled("accessToken"), "/me");
    }

    @And("Correct user ID should be retrieved")
    public void correctUserIDShouldBeRetrieved() {
        userDetailsVerification.verifyUserId(lastResponse(), UserId.valid.getValue());
    }
}
