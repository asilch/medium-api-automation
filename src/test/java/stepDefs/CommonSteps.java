package stepDefs;

import request.Request;
import verification.Verification;
import data.AccessToken;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class CommonSteps {
    @Steps
    Request request;

    @Steps
    Verification verification;

    @Given("I have valid accessToken")
    public void iHaveValidAccessToken() {
        Serenity.setSessionVariable("accessToken").to(AccessToken.valid.getValue());
    }

    @Given("I have invalid accessToken")
    public void iHaveInvalidAccessToken() {
        Serenity.setSessionVariable("accessToken").to(AccessToken.invalid.getValue());
    }

    @Given("I have no accessToken")
    public void iHaveNoAccessToken() {
        Serenity.setSessionVariable("accessToken").to(AccessToken.no.getValue());
    }
    @And("Correct message {string} and code {int} should be received")
    public void correctMessageAndCodeShouldBeReceived(String message, int code) {
        verification.verifyMessage(lastResponse(), message);
        verification.verifyCode(lastResponse(), code);
    }
    @Then("Status code {int} should be received")
    public void statusCodeShouldBeReceived(Integer statusCode) {
        verification.verifyStatusCode(statusCode, lastResponse());
    }

    @And("Response schema should match the specification {string}")
    public void responseSchemaShouldMatchTheSpecification(String jsonSchema) {
        verification.verifyResponseSchema(jsonSchema, lastResponse());
    }
}
