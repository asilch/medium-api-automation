package stepDefs;

import request.Request;
import verification.PublicationsVerification;
import verification.Verification;
import data.UserId;
import io.cucumber.java.en.*;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class PublicationsStep {
    @Steps
    Request request;

    @Steps
    PublicationsVerification verification;

    @When("I try to get a list of publications using valid id")
    public void iTryToGetAListOfPublicationsUsingValidId() {
        request.get(Serenity.sessionVariableCalled("accessToken"), "/users/"+ UserId.valid.getValue() +"/publications");
    }

    @When("I try to get a list of publications another user's id")
    public void iTryToGetAListOfPublicationsAnotherUserSId() {
        request.get(Serenity.sessionVariableCalled("accessToken"), "/users/"+ UserId.anotherUserId.getValue() +"/publications");
    }

    @When("I try to get a list of publications using invalid id")
    public void iTryToGetAListOfPublicationsUsingInvalidId() {
        request.get(Serenity.sessionVariableCalled("accessToken"), "/users/"+ UserId.invalid.getValue() +"/publications");
    }

    @And("Correct publication ID {string} should be retrieved")
    public void correctPublicationIDShouldBeRetrieved(String publicationId) {
        verification.verifyPublicationId(lastResponse(), publicationId);
    }
}
