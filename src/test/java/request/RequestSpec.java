package request;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class RequestSpec {
    public static RequestSpecification setSpec() {
        return new RequestSpecBuilder().setBaseUri("https://api.medium.com/v1")
                .build();
    }
}
