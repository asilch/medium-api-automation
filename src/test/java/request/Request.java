package request;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

public class Request {
    public Response get(String accessToken, String path) {
        var resp = SerenityRest.given().spec(RequestSpec.setSpec())
                .basePath(path);
        if (!accessToken.isEmpty()) {
            resp.auth().oauth2(accessToken);
        }
        return resp.get().then().extract().response();
    }
}
