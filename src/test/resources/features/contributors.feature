Feature: Get a list of users who are allowed to publish under a publication(contributors)
  As an User
  I should be able to get a list of contributors for a given publication and contributor's details

  Scenario: User gets a list of contributors of an existing publication
    Given I have valid accessToken
    When I try to get a list of contributors for publication with ID "b5717b16a765"
    Then Status code 200 should be received
    And Response schema should match the specification "contributors.json"
    And Correct publication ID should be retrieved

  Scenario: User gets an empty list of contributors of not existing publication
    Given I have valid accessToken
    When I try to get a list of contributors for publication with ID "1"
    Then Status code 200 should be received
    And An empty list of contributors should be retrieved

  Scenario: User gets an error code and message using invalid accessToken while requesting a list of contributors
    Given I have invalid accessToken
    When I try to get a list of contributors for publication with ID "b5717b16a765"
    Then Status code 401 should be received
    And Correct message "Token was invalid." and code 6003 should be received

  Scenario: User gets an error code and message without using accessToken while requesting a list of contributors
    Given I have no accessToken
    When I try to get a list of contributors for publication with ID "b5717b16a765"
    Then Status code 401 should be received
    And Correct message "An access token is required." and code 6000 should be received
