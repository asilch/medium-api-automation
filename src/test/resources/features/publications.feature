Feature: Get a list of publications the user is subscribed to or writes to or edits
  As an User
  I should be able to get a list of publications

  Scenario: User gets a list of publications using his valid id
    Given I have valid accessToken
    When I try to get a list of publications using valid id
    Then Status code 200 should be received
    And Response schema should match the specification "publications.json"
    And Correct publication ID "815791d4a11c" should be retrieved

  Scenario: User gets an error message using valid another user's id while requesting a list of publications
    Given I have valid accessToken
    When I try to get a list of publications another user's id
    Then Status code 403 should be received
    And Correct message "You may not list publications for another user" and code -1 should be received

  Scenario: User gets an error message using invalid id while requesting a list of publications
    Given I have valid accessToken
    When I try to get a list of publications using invalid id
    Then Status code 400 should be received
    And Correct message "userId was invalid." and code 6026 should be received

  Scenario: User gets an error code and message using invalid accessToken while requesting a list of publications
    Given I have invalid accessToken
    When I try to get a list of publications using valid id
    Then Status code 401 should be received
    And Correct message "Token was invalid." and code 6003 should be received

  Scenario: User gets an error code and message without using accessToken while requesting a list of publications
    Given I have no accessToken
    When I try to get a list of publications using valid id
    Then Status code 401 should be received
    And Correct message "An access token is required." and code 6000 should be received
