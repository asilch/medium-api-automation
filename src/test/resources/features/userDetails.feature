Feature: Get details of the user who has granted permission to the application
  As a User
  I should be able to retrieve my details

  Scenario: User gets details using valid accessToken
    Given I have valid accessToken
    When I try to get user details
    Then Status code 200 should be received
    And Response schema should match the specification "userDetails.json"
    And Correct user ID should be retrieved

  Scenario: User gets an error code and message using invalid accessToken while requesting user details
    Given I have invalid accessToken
    When I try to get user details
    Then Status code 401 should be received
    And Correct message "Token was invalid." and code 6003 should be received

  Scenario: User gets an error code and message without using accessToken while requesting user details
    Given I have no accessToken
    When I try to get user details
    Then Status code 401 should be received
    And Correct message "An access token is required." and code 6000 should be received
