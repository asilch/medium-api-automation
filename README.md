**Java-Serenity-RestAssured-Cucumber-JUnit-Gradle API Automation Solution**

**Introduction**
This is the rest API test automation solution for endpoints documented in https://any-api.com/medium_com/medium_com/docs/API_Description. The published Medium’s API is a JSON-based OAuth2 RESTful API that arranged around resources.
Tests are written using a combination of SerenityBDD, RestAssured, Cucumber, Junit & Gradle.
For now such resources as users and publications have been automated. Posts and images resources will be automated soon.

**Design Considerations**
Tests are written in BDD Gherkin format in Cucumber feature files and it is represented as a living documentation in the test report.
For test running CucumberWithSerenity logic is used.
Test scenarios to validate API response schema have been included for each endpoint in the respective feature file. The API spec for schema comparison is placed inside "schema" folder in test resources.
To create tests for new resource new feature file with scenarios needed to be written. Then written scenarios should be implemented as steps in stepDef directory. For common steps used in multiple scenarios please use CommonSteps class. After step defifnition request and response verification logic should be implemented. All logic connected with making request should be written inside request directory, response verification logic should be written inside verification directory.

**Executing the tests**
To run test in GitLab CI please run the pipeline.
After the pipeline execution test report will be available at https://asilch.gitlab.io/medium-api-automation/index.html



